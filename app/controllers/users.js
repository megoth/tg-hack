/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  https = require('https'),
  markdown = require('markdown-js').markdown,
  _ = require('underscore'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  promise = require('node-promise');

/**
 * Find user by id
 */
exports.user = function (req, res, next, username) {
  User.loadByName(username, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next(new Error('Failed to load User ' + username));
    }
    req.profile = user;
    return next();
  });
};

exports.invitedUser = function (req, res, next, inviteToken) {
  User
    .findOne({inviteToken: inviteToken})
    .exec(function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(new Error('Failed to load User with inviteToken ' + inviteToken));
      }
      req.profile = user;
      return next();
    });
};

/**
 *
 * @param req
 * @param res
 */
exports.achievement = function (req, res) {
  var achievement = req.achievement,
    user = req.profile;
  achievement.body = markdown(achievement.body);
  res.render('users/achievement', {
    achievement: achievement,
    user: user,
    isUser: req.isAuthenticated() && req.user.id === user.id,
    hasAchievement: user.hasAchievement(achievement),
    facebook: {
      appId: config.facebook.appId,
      appName: config.facebook.appName,
      channelUrl: config.app.uri+"/facebook/channel.html",
      // for send button
      title: achievement.title,
      type: "game",
      url: config.app.uri+"/users/"+user.username+"/achievements/"+achievement._id,
      image: config.app.uri+"/images/achievements/"+achievement.image,
      site_name: config.app.name,
      admins: config.facebook.adminUserId,
      app_id: config.facebook.app_id,
      description: user.username+" accomplished the achievement "+achievement.title+"!"
    }
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.assignment = function (req, res) {
  var assignment = req.assignment,
    user = req.profile;
  assignment.body = markdown(assignment.body);
  res.render('users/assignment', {
    assignment: assignment,
    user: user,
    isUser: req.isAuthenticated() && req.profile.id === user.id,
    hasAssignment: user.hasAssignment(assignment),
    meetRequirements: req.user.canDoAssignment(assignment),
    categoryClass: assignment.getCategoryClass()
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.delete = function (req, res) {
  res.render('users/delete', {
    title: 'Delete ' + req.profile.username,
    user: req.profile
  });
};

/**
 * Destroy a user
 */

exports.destroy = function (req, res) {
  var user = req.profile;
  user.remove(function () {
    req.flash('notice', 'Deleted successfully');
    res.redirect('/users')
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.edit = function (req, res) {
  res.render('users/edit', {
    user: req.profile
  })
};

exports.index = function (req, res) {
  User.list(function (err, users) {
    if (err) {
      res.render('500');
    }
    res.render('users/index', {
      title: "List of users",
      users: users
    });
  });
};

exports.list = function (req, res) {
  User.find({})
    .where('points').gt(0)
    .sort({points: -1, lastAssignmentSolved: 1})
    .exec(function (err, users) {
      if (err) {
        return res.render('500');
      }
      return res.render('users/list', {
        users: users
      });
    });
};

exports.login = function (req, res) {
  res.render('users/login');
};

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout();
  res.redirect('/');
};

exports.recalculate = function (req, res) {
  res.render("users/recalculate", {
    user: req.profile
  });
};

exports.calculate = function (req, res) {
  var user = req.profile,
    assignments = _.clone(user.assignments),
    promises = [];
  user.categories.forEach(function () {
    user.categories.pop();
  });
  assignments.forEach(function () {
    promises.push(new promise.Promise());
    user.assignments.pop();
  });
  user.points = 0;
  assignments.forEach(function (assignment, i) {
    user.addAssignment(assignment, {
      onSuccess: function (u) {
        promises[i].resolve();
        _.extend(user, u);
      },
      onError: function () {
        req.flash("error", "Failed recalculate user");
        return res.redirect('/users');
      },
      onRequirementsNotMet: function () {
        req.flash("error", "Failed recalculate user (didn't meet requirement)");
        return res.redirect('/users');
      }
    })
  });
  promise.all(promises).then(function () {
    user.save(function (err) {
      if (err) {
        return res.render('500');
      }
      req.flash("success", "user level recalculated!");
      return res.redirect('/users');
    });
  });
};

exports.show = function (req, res) {
  if (req.profile.private) {
    req.flash('error', "Profile is private");
    return res.redirect('/');
  }
  var categoryMap = {},
    categories = [];
  req.profile.categories.forEach(function (uc, i) {
    if (categoryMap[uc.category._id] && categoryMap[uc.category._id].level < uc.level) {
      categories[categoryMap[uc.category._id].index] = uc;
      categoryMap[uc.category._id].level = uc.level;
    } else if (!categoryMap[uc.category._id]) {
      categoryMap[uc.category._id] = {
        index: i,
        level: uc.level
      };
      categories.push(uc);
    }
  });
  req.profile.categories = categories;
  return res.render('users/show', {
    user: req.profile
  });
};

exports.update = function (req, res) {
  var profile = _.extend(req.profile, req.body);
  profile.isAdmin = !!req.body.isAdmin;
  profile.hasTicket = !!req.body.hasTicket;
  profile.isWannabe = !!req.body.isWannabe;
  profile.rewardsAsus = !!req.body.rewardsAsus;
  var achievements = req.body.achievements;
  if (achievements && _.isArray(achievements)) {
    profile.level = achievements.length;
  } else if (achievements) {
    profile.level = 1;
  } else {
    profile.achievements.forEach(function () {
      profile.achievements.pop();
    });
    profile.level = 0;
  }
  profile.save(function (err) {
    if (err) {
      return res.render('achievements/edit', {
        title: 'Edit Achievement',
        user: profile,
        errors: err.errors
      });
    }
    return res.redirect('/users');
  });
};