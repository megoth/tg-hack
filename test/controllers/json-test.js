/*global assert, refute, require*/

var config = require('../../config/config')["test"],
  buster = require("buster"),
  sinon = require("sinon"),
  mongoose = require("mongoose"),
  promise = require("node-promise"),
  json = require("../../app/controllers/json"),
  userSave = require("../models/users").save,
  userSetup = require("../models/users").setUp,
  userTeardown = require("../models/users").teardown;

require("../../app/models/user");

// setup models
var User = mongoose.model('User');

buster.testCase("JSON API", {
  setUp: function () {
    this.jsonSpy = sinon.spy();
    this.req = {};
    this.res = {
      json: this.jsonSpy
    };
  },
  "Profile": {
    setUp: function (done) {
      userSetup.call(this, done(function () {
        this.req.user = this.user;
        json.profile(this.req, this.res);
      }.bind(this)));
    },
    tearDown: userTeardown,
    "//should call on res.json": function () {
      assert(this.jsonSpy.called);
    },
    "//should call with correct data": {
      setUp: function () {
        this.args = this.jsonSpy.getCall(0).args[0];
      },
      "no level set": function () {
        assert.equals(this.args.level, 0);
      },
      "all categories set, but all to zero": function () {
        assert.equals(this.args.categories.length, 1);
      }
    }
  }
});