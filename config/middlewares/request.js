/**
 * Module dependencies.
 */
var http = require('http'),
  req = http.IncomingMessage.prototype;

req.isAdmin = function () {
  return this.user && this.user.isAdmin;
};
