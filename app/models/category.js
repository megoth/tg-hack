/*global process, require*/

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  _ = require("underscore"),
  Schema = mongoose.Schema;

// Assignment schema

var CategorySchema = new Schema({
  title: {type: String, required: true},
  shortTitle: {type: String, required: true},
  abbreviation: {type:String, required: true},
  description: {type: String, required: true, default: ""},
  order: {type:Number, required: true, default: 0}
});

/**
 * Statics
 */

CategorySchema.statics = {

  /**
   * Find category by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api public
   */

  load: function (id, cb) {
    this.findOne({ _id: id }).exec(cb);
  },

  /**
   * List categories
   *
   * @param {Function} cb
   * @api public
   */

  list: function (cb) {
    this
      .find({})
      .sort({'order': 1})
      .execFind(cb)
  },

  /**
   *
   */
  listWithAssignment: function (assignment, cb) {
    this.find({})
      .exec(function (err, categories) {
        categories.forEach(function (category) {
          category.checked = _.where(assignment.categories, { id: category.id }).length > 0;
        });
        cb(err, categories);
      });
  }
};

exports.CategorySchema = CategorySchema;
mongoose.model('Category', CategorySchema);
