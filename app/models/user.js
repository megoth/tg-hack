/*global require*/

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  _ = require('underscore'),
  querystring = require('querystring'),
  promise = require('node-promise'),
  https = require('https'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config.js')[env],
  spawn = require('child_process').spawn;

require("./assignment");

//noinspection JSUnresolvedFunction
var Assignment = mongoose.model("Assignment");

/**
 * User Schema
 */

var UserSchema = new Schema({
  name: {type: String, required: true},
  username: {type: String, required: true, unique: true},
  geekeventsId: {type: Number, unique: true},
  geekeventsTimestamp: {type: Number},
  geekeventsToken: {type: String},
  isAdmin: {type:Boolean, default: false},
  hasTicket: {type:Boolean, default: false},
  isWannabe: {type:Boolean, default: false},
  ipAddress: {type: String, required: true},
  userAgent: {type: String, required: true},
  points: {type: Number, default: 0},
  achievements: [
    {type: Schema.ObjectId, ref: 'Achievement'}
  ],
  assignments: [
    {type: Schema.ObjectId, ref: 'Assignment'}
  ],
  lastAssignmentSolved: {type: Number, default: 0},
  level: {type: Number, default: 0},
  categories: [
    {
      category: {type: Schema.ObjectId, ref: 'Category'},
      level: {type: Number, default: 0}
    }
  ],
  private: {type:Boolean, default: false},
  rewardsAsus: {type:Boolean, default: false}
});

UserSchema.index({ username: 1 });
UserSchema.index({ geekeventsId: 1 });
UserSchema.index({ wannabeUsername: 1 });

/**
 * Validations
 */

UserSchema.path('name').validate(function (name) {
  return name.length > 0;
}, 'Name cannot be blank');

UserSchema.path('username').validate(function (username) {
  return username.length > 0;
}, 'Username cannot be blank');

/**
 * Methods
 */

UserSchema.methods = {
  hasAccess: function () {
    return this.isAdmin || this.isWannabe || this.hasTicket;
  },
  /**
   *
   * @return {Boolean}
   */
  hasAssignment: function (assignment) {
    return _.where(this.assignments, {id: assignment.id}).length > 0;
  },
  canDoAssignment: function (assignment) {
    var i = 0,
      requirements = assignment.requirements,
      numRequirements = requirements.length,
      meetRequirements = true;
    while(meetRequirements && i < numRequirements) {
      meetRequirements = _.where(this.assignments, {id: requirements[i].id}).length > 0;
      i++;
    }
    return this.hasAccess() && meetRequirements;
  },
  /**
   *
   */
  addAssignment: function (assignment, fns) {
    var user = this,
      isSolved = user.hasAssignment(assignment),
      meetRequirements = user.canDoAssignment(assignment);
    fns = _.extend({
      onSuccess: function () {},
      onAlreadySolved: function () {},
      onRequirementsNotMet: function () {}
    }, fns);
    if (isSolved) {
      return fns.onAlreadySolved();
    }
    if (!meetRequirements) {
      return fns.onRequirementsNotMet();
    }
    this.assignments.push(assignment);
    this.points += assignment.points;
    this.lastAssignmentSolved = new Date().getTime();
    assignment.categories.forEach(function (category) {
      var categories = user.categories.filter(function (userCategory) {
        return userCategory.category.id === category.id;
      });
      var curIndex = 0;
      if (categories.length === 0) {
        return user.categories.push({
          category: category,
          level: 1
        });
      } else if (categories.length > 1) {
        categories.forEach(function (c, i) {
          curIndex = c.level > user.categories[0].level ? i : curIndex;
        });
      }
      return categories[curIndex].level += assignment.level === 1 ? 1 : 0;
    });
    fns.onSuccess(this);
  },
  hasAchievement: function (achievement) {
    return _.where(this.achievements, {id: achievement.id}).length > 0;
  },
  canGetAchievement: function (achievement) {
    var user = this,
      i = 0,
      requirements = achievement.requirements,
      numRequirements = requirements.length,
      requirementsAreMet = true;
    while (requirementsAreMet && i < numRequirements) {
      requirementsAreMet = _.reduce(requirements[i].assignments, function (memo, assignment) {
        return memo && _.filter(user.assignments, function (a) {
          return a.id == assignment;
        }).length > 0;
      }, true) && user.points >= requirements[i].points;
      i++;
    }
    return requirementsAreMet;
  },
  /**
   *
   * @param achievement
   * @param fns
   */
  addAchievement: function (achievement, fns) {
    var user = this,
      hasAchievement = this.hasAchievement(achievement),
      canGetAchievement = this.canGetAchievement(achievement),
      origSuccess = fns.onSuccess;
    fns = _.extend({
      onSuccess: function () {},
      onAlreadyAchieved: function () {},
      onRequirementFailed: function () {}
    }, fns, {
      onSuccess: function (user) {
        user.achievements.push(achievement);
        user.level += 1;
        origSuccess(user);
      }
    });
    if (hasAchievement) {
      return fns.onAlreadyAchieved();
    }
    if (canGetAchievement) {
      return fns.onSuccess(user);
    }
    return fns.onRequirementFailed();
  },
  genVpn: function () {
    var vpn = spawn(config.genVpn.script, [this.geekeventsId, this.username]);
    vpn.stdout.on('data', function (data) {
      console.log('genVpn > ' + data);
    });
    vpn.on('close', function (code) {
      console.log('genVpn closed with ' + code);
    });
  }
};

/**
 * Statics
 */

UserSchema.statics = {

  /**
   * Find requirement by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api public
   */

  load: function (id, cb) {
    this.findOne({ _id: id })
      .populate('categories.category')
      .populate('assignments')
      .populate('achievements')
      .exec(cb);
  },

  loadByName: function (username, cb) {
    this.findOne({ username:username })
      .populate('categories.category')
      .populate('assignments')
      .populate('achievements')
      .exec(cb);
  },

  /**
   *
   * @param cb
   */
  list: function (cb) {
    this.find()
      .order({username: 1})
      .exec(cb);
  },
  listFromAchievement: function (achievement, cb) {
    this.find({private:false})
      .where('achievements').in([achievement._id])
      .exec(cb);
  },
  /**
   * List of users who've completed the given assignment
   * @param assignment
   * @param cb
   */
  listFromAssignment: function (assignment, cb) {
    this.find({private:false})
      .where('assignments').in([assignment._id])
      .exec(cb);
  },
  listHighScore: function (cb) {
    this.find({})
      .where('points').gt(0)
      .sort({points: -1, lastAssignmentSolved: 1})
      .limit(10)
      .exec(cb);
  }
};

mongoose.model('User', UserSchema);
