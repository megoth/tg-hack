var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  async = require('async'),
  querystring = require('querystring');

module.exports = function (app, passport, auth) {
  var env = process.env.NODE_ENV || 'development',
    config = require('./config')[env];

  var users = require('../app/controllers/users');

  // user routes
  app.get('/login', users.login);
  app.get('/logout', users.logout);
  app.get('/users', auth.isAdmin, users.index);
  app.get('/users/:username/delete', auth.isAdmin, users.delete);
  app.get('/users/:username/edit', auth.isAdmin, users.edit);
  app.put('/users/:username', auth.isAdmin, users.update);
  app.del('/users/:username', auth.isAdmin, users.destroy);
  app.get('/users/:username/recalculate', auth.isAdmin, users.recalculate);
  app.post('/users/:username/recalculate', auth.isAdmin, users.calculate);

  // user routes (public)
  app.get('/users/:username', users.show);
  app.get('/users/:username/achievements/:achievementId', users.achievement);
  app.get('/users/:username/assignments/:assignmentId', users.assignment);
  app.get('/list/users', users.list);

  // json api
  var json = require('../app/controllers/json');
  app.get('/json/spider/:username', json.spider);

  app.param('username', users.user);

  // login and auth for the geekevent API
  var geekevents = require("../app/controllers/geekevents");
  app.get('/geekevents', auth.requiresUnauthorized, passport.authenticate('geekevents', {
    failureRedirect: 'https://www.geekevents.org/sso/?next='+config.geekevents.redirect+'/geekevents/auth'
  }), geekevents.login);
  app.get('/geekevents/auth', geekevents.auth);
  app.post('/geekevents/auth', geekevents.signup);
  app.post('/geekevents/signup', geekevents.signup);

  // profile routes
  var profiles = require('../app/controllers/profiles');
  app.get('/profile', auth.requiresLogin, profiles.view);
  app.get('/profile/delete', auth.requiresLogin, profiles.delete);
  app.get('/profile/edit', auth.requiresLogin, profiles.edit);
  app.put('/profile', auth.requiresLogin, profiles.update);
  app.del('/profile', auth.requiresLogin, profiles.destroy);

  // achievement routes
  var achievements = require('../app/controllers/achievements');
  app.get('/achievements', auth.isAdmin, achievements.index);
  app.get('/achievements/new', auth.isAdmin, achievements.new);
  app.post('/achievements', auth.isAdmin, achievements.create);
  app.get('/achievements/:achievementId', achievements.show);
  app.get('/achievements/:achievementId/delete', auth.isAdmin, achievements.delete);
  app.get('/achievements/:achievementId/edit', auth.isAdmin, achievements.edit);
  app.get('/achievements/:achievementId/stats', achievements.stats);
  app.put('/achievements/:achievementId', auth.isAdmin, achievements.update);
  app.del('/achievements/:achievementId', auth.isAdmin, achievements.destroy);

  app.param('achievementId', achievements.achievement);

  // requirement routes
  var requirements = require('../app/controllers/requirements');
  app.get('/achievements/:achievementId/requirements/new', auth.isAdmin, requirements.new);
  app.post('/achievements/:achievementId/requirements', auth.isAdmin, requirements.create);
  app.get('/achievements/:achievementId/requirements/:requirementId/delete', auth.isAdmin, requirements.delete);
  app.get('/achievements/:achievementId/requirements/:requirementId/edit', auth.isAdmin, requirements.edit);
  app.put('/achievements/:achievementId/requirements/:requirementId', auth.isAdmin, requirements.update);
  app.del('/achievements/:achievementId/requirements/:requirementId', auth.isAdmin, requirements.destroy);
  app.get('/achievements/:achievementId/requirements/:requirementId/assignments/add', auth.isAdmin, requirements.addAssignment);
  app.post('/achievements/:achievementId/requirements/:requirementId/assignments', auth.isAdmin, requirements.pushAssignment);

  app.param('requirementId', requirements.requirement);

  // assignment routes
  var assignments = require('../app/controllers/assignments');
  app.get('/assignments', auth.isAdmin, assignments.index);
  app.get('/assignments/new', auth.isAdmin, auth.requiresLogin, assignments.new);
  app.post('/assignments', auth.isAdmin, assignments.create);
  app.get('/assignments/:assignmentId', assignments.show);
  app.get('/assignments/:assignmentId/delete', auth.isAdmin, assignments.delete);
  app.get('/assignments/:assignmentId/edit', auth.isAdmin, assignments.edit);
  app.post('/assignments/:assignmentId', auth.requiresLogin, assignments.answer);
  app.get('/assignments/:assignmentId/stats', assignments.stats);
  app.put('/assignments/:assignmentId', auth.isAdmin, assignments.update);
  app.del('/assignments/:assignmentId', auth.isAdmin, assignments.destroy);

  app.param('assignmentId', assignments.assignment);

  // category routes
  var categories = require('../app/controllers/categories');
  app.get('/categories', auth.isAdmin, categories.index);
  app.get('/categories/new', auth.isAdmin, categories.new);
  app.post('/categories', auth.isAdmin, categories.create);
  app.get('/categories/final', categories.final);
  app.get('/categories/mixed', categories.mixed);
  app.get('/categories/:categoryId', categories.show);
  app.get('/categories/:categoryId/delete', auth.isAdmin, categories.delete);
  app.get('/categories/:categoryId/edit', auth.isAdmin, categories.edit);
  app.put('/categories/:categoryId', auth.isAdmin, categories.update);
  app.del('/categories/:categoryId', auth.isAdmin, categories.destroy);

  app.param('categoryId', categories.category);

  // home route
  var frontpage = require('../app/controllers/frontpage');
  app.get('/', frontpage.index);

  // info routes
  var texts = require('../app/controllers/texts');
  app.get('/image', texts.image);
  app.get('/progress-graph', texts.progressGraph);
  app.get('/note/assignments', texts.assignments);
  app.get('/vm', texts.vm);
  app.get('/vpn', texts.vpn);

  var vpn = require('../app/controllers/vpn');
  app.get('/vpn/download', auth.requiresLogin, vpn.download);
};
