//noinspection JSUnresolvedFunction

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  _ = require('underscore'),
  promise = require('node-promise'),
  Assignment = mongoose.model('Assignment'),
  Category = mongoose.model('Category'),
  User = mongoose.model('User');

/**
 * display frontpage
 */

exports.spider = function (req, res) {
  var category,
    response = {
      level:req.profile.level,
      categories:[]
    },
    categoryMap = {};

  Assignment.list({level:1}, function (err, assignments) {
    assignments.forEach(function (assignment) {
      category = categoryMap[assignment.categories[0]._id] || {
        abbr: assignment.categories[0].abbreviation,
        title: assignment.categories[0].title,
        level: 0,
        max: 0
      };
      category.max += 1;
      categoryMap[assignment.categories[0]._id] = category;
    });
    req.profile.categories.forEach(function (userCategory) {
      if (categoryMap[userCategory.category._id]) {
        categoryMap[userCategory.category._id].level = userCategory.level;
      }
    });
    Object.keys(categoryMap).forEach(function (categoryKey, index) {
      response.categories.push({
        key: "a",
        value: categoryMap[categoryKey].level,
        time: index,
        abbr: categoryMap[categoryKey].abbr
      });
      response.categories.push({
        key: "b",
        value: categoryMap[categoryKey].max - categoryMap[categoryKey].level,
        time: index,
        abbr: categoryMap[categoryKey].abbr
      });
    });
    res.json(response);
  });
};