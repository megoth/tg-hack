#include<stdio.h>

int main( int argc, char *argv[] ) {
    if (argc != 3) {
        printf("Wrong number of args given! Was %d...\n", argc);
        return -1;
    }
    printf("VPN zip for %s created!\n", argv[1]);
}
