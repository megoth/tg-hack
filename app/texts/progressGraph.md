# The progress graph

![An example of the progress graph](/images/progress.png)

The progress bar shows your (or a given user) progress in the competition. Centered is the hacker level, while the progress in the different categories are displayed circling the hacker level.

As tasks are completed, the progress is shown in the inner circle. The total levels are displayed in the outer circle.