/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  async = require('async'),
  Assignment = mongoose.model('Assignment'),
  Category = mongoose.model('Category'),
  _ = require('underscore'),
  markdown = require('markdown-js').markdown;

/**
 * Find category by id
 */

exports.category = function (req, res, next, id) {
  Category.load(id, function (err, category) {
    if (err) {
      return next(err);
    }
    if (!category) {
      return next(new Error('Failed to load category ' + id));
    }
    req.category = category;
    return next();
  });
};

/**
 * Create an category
 */

exports.create = function (req, res) {
  var category = new Category(req.body);
  category.user = req.user;

  category.save(function (err) {
    if (err) {
      res.render('categories/new', {
        title:'New Category',
        category:category,
        errors:err.errors
      });
    } else {
      res.redirect('/categories/' + category._id);
    }
  });
};

/**
 * Delete an category
 */

exports.delete = function (req, res) {
  res.render('categories/delete', {
    title:'Delete ' + req.category.title,
    category:req.category
  });
};

/**
 * Destroy an category
 */

exports.destroy = function (req, res) {
  var category = req.category;
  category.remove(function (err) {
    if (err) {
      res.redirect('/categories/' + category._id + '/delete');
    }
    req.flash('notice', 'Deleted successfully');
    res.redirect('/categories')
  });
};

/**
 * Edit an category
 */

exports.edit = function (req, res) {
  res.render('categories/edit', {
    title:'Edit ' + req.category.title,
    category:req.category
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.final = function (req, res) {
  Assignment.list({level: 3}, function (err, assignments) {
    if (err || assignments.length === 0) {
      return res.render('500');
    }
    return res.redirect('/assignments/'+assignments[0]._id);
  })
};

/**
 * List of categories
 */

exports.index = function (req, res) {
  Category.list(function (err, categories) {
    if (err) {
      return res.render('500')
    }
    return res.render('categories/index', {
      title:'List of Categories',
      categories:categories
    });
  });
};

exports.mixed = function (req, res) {
  Assignment.list({level: 2}, function (err, assignments) {
    if (err) {
      return res.render('500');
    }
    return res.render('categories/show', {
      title:"Mixed assignments",
      category:new Category({
        title:"Mixed",
        description:"This is a collection of assignments"
      }),
      categoryClass:"mixed",
      assignments: assignments
    });
  })
};

/**
 * New category
 */

exports.new = function (req, res) {
  res.render('categories/new', {
    title:'New Category',
    category:new Category({})
  });
};

/**
 * View an category
 */

exports.show = function (req, res) {
  var category = req.category;
  category.description = markdown(category.description);
  Assignment.listFromCategory(category._id, function (err, assignments) {
    res.render('categories/show', {
      title:req.category.title,
      category:category,
      categoryClass:"normal",
      assignments: assignments
    });
  });
};

/**
 * Update category
 */

exports.update = function (req, res) {
  var category = req.category;
  category = _.extend(category, req.body);

  category.save(function (err) {
    if (err) {
      res.render('categories/edit', {
        title:'Edit Category',
        category:category,
        errors:err.errors
      });
    } else {
      res.redirect('/categories/' + category._id)
    }
  });
};
