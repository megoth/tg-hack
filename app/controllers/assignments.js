/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  async = require('async'),
  _ = require('underscore'),
  promise = require('node-promise'),
  markdown = require('markdown-js').markdown,
  Achievement = mongoose.model('Achievement'),
  Assignment = mongoose.model('Assignment'),
  Category = mongoose.model('Category'),
  User = mongoose.model('User');

/**
 * Find assignment by id
 */

exports.assignment = function (req, res, next, id) {
  Assignment.load(id, function (err, assignment) {
    if (err) {
      return next(err);
    }
    if (!assignment) {
      return next(new Error('Failed to load assignment ' + id));
    }
    req.assignment = assignment;
    return next();
  });
};

exports.answer = function (req, res) {
  var assignment = req.assignment,
    user = req.user;
  if (assignment.token !== req.body.token) {
    req.flash('error', 'Wrong token!');
    return res.redirect('/assignments/' + assignment._id);
  }
  return user.addAssignment(assignment, {
    onSuccess: function (user) {
      Achievement.list(function (err, achievements) {
        var newAchievements = [];
        achievements.forEach(function (achievement) {
          user.addAchievement(achievement, {
            onSuccess: function () {
              newAchievements.push(achievement);
            }
          });
        });
        user.save(function (err, user) {
          if (err) {
            return res.render('500');
          }
          req.flash('notice', 'Success!');
          req.flash('achievements', newAchievements);
          return res.redirect('/assignments/' + assignment._id + '/stats');
        });
      });
    },
    onAlreadySolved: function () {
      req.flash('error', 'Assignment already solved');
      res.redirect('/assignments/' + assignment._id);
    },
    onRequirementsNotMet: function () {
      req.flash('error', 'Requirements not met');
      res.redirect('/assignments/' + assignment._id);
    }
  });
};

/**
 * Create an assignment
 */

exports.create = function (req, res) {
  var assignment = new Assignment(req.body);

  assignment.save(function (err) {
    if (err) {
      res.render('assignments/new', {
        title: 'New Assignment',
        assignment: assignment,
        errors: err.errors
      });
    } else {
      res.redirect('/assignments/' + assignment._id);
    }
  });
};

/**
 * Delete an assignment
 */

exports.delete = function (req, res) {
  res.render('assignments/delete', {
    title: 'Delete ' + req.assignment.title,
    assignment: req.assignment
  });
};

/**
 * Destroy an assignment
 */

exports.destroy = function (req, res) {
  var assignment = req.assignment;
  assignment.remove(function (err) {
    if (err) {
      return res.render('500');
    }
    req.flash('notice', 'Deleted successfully');
    return res.redirect('/assignments');
  });
};

/**
 * Edit an assignment
 */

exports.edit = function (req, res) {
  var categoryPromise = new promise.Promise(),
    requirementPromise = new promise.Promise();
  Category.listWithAssignment(req.assignment, function (err, categories) {
    if (err) {
      return categoryPromise.reject(err);
    }
    return categoryPromise.resolve(categories);
  });
  Assignment.listRequirement(req.assignment, function (err, requirements) {
    if (err) {
      return requirementPromise.reject(err);
    }
    return requirementPromise.resolve(requirements);
  });
  promise.all(categoryPromise, requirementPromise)
    .then(function (promises) {
      res.render('assignments/edit', {
        title: 'Edit ' + req.assignment.title,
        assignment: req.assignment,
        categories: promises[0],
        requirements: promises[1]
      });
    })
};

/**
 * List of Assignments
 */

exports.index = function (req, res) {
  Assignment.list({}, function (err, assignments) {
    if (err) {
      return res.render('500');
    }
    return res.render('assignments/index', {
      title: 'List of Assignments',
      assignments: assignments
    });
  });
};

/**
 * New assignment
 */

exports.new = function (req, res) {
  var categoryPromise = new promise.Promise(),
    requirementPromise = new promise.Promise();
  Category.list(function (err, categories) {
    if (err) {
      return categoryPromise.reject(err);
    }
    return categoryPromise.resolve(categories);
  });
  Assignment.list({}, function (err, requirements) {
    if (err) {
      return requirementPromise.reject(err);
    }
    return requirementPromise.resolve(requirements);
  });
  promise.all(categoryPromise, requirementPromise)
    .then(function (promises) {
      res.render('assignments/new', {
        title: 'New Assignment',
        assignment: new Assignment({}),
        categories: promises[0],
        requirements: promises[1]
      });
    });
};

/**
 * View an assignment
 */

exports.show = function (req, res) {
  var assignment = req.assignment,
    user = req.user || new User({});
  assignment.body = markdown(assignment.body);
  res.render('assignments/show', {
    title: assignment.title,
    assignment: assignment,
    user: user,
    isSolved: user.hasAssignment(assignment),
    meetRequirements: user.canDoAssignment(assignment),
    categoryClass: assignment.getCategoryClass()
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.stats = function (req, res) {
  var assignment = req.assignment,
    user = req.user || new User({}),
    achievements = req.flash("achievements")[0],
    newAchievement = achievements && achievements.length > 0;
  assignment.body = markdown(assignment.body);
  User.listFromAssignment(assignment, function (err, users) {
    if (err) {
      req.flash('error', "Error while loading users");
      return res.redirect('/assignments/'+assignment._id);
    }
    return res.render('assignments/stats', {
      title: assignment.title,
      assignment: assignment,
      user: user,
      users: users,
      achievements: achievements,
      newAchievement: newAchievement,
      isSolved: user.hasAssignment(assignment),
      meetRequirements: user.canDoAssignment(assignment),
      categoryClass: assignment.getCategoryClass()
    });
  });
};

/**
 * Update assignment
 */

exports.update = function (req, res) {
  var assignment = req.assignment;
  if (!req.body.requirements) {
    assignment.requirements.forEach(function () {
      assignment.requirements.pop();
    });
  }
  _.extend(assignment, req.body);
  assignment.save(function (err) {
    if (err) {
      res.render('assignments/edit', {
        title: 'Edit Assignment',
        assignment: assignment,
        errors: err.errors
      });
    } else {
      res.redirect('/assignments/' + assignment._id)
    }
  });
};