//noinspection JSUnresolvedFunction

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  _ = require('underscore'),
  promise = require('node-promise'),
  Category = mongoose.model('Category'),
  User = mongoose.model('User');

/**
 * display frontpage
 */

exports.index = function (req, res) {
  User.listHighScore(function (err, users) {
    if (err) {
      return res.render('500')
    }
    Category.list(function (err, categories) {
      if (err) {
        return res.render('500')
      }
      return res.render('frontpage/index', {
        title: 'The Gathering Hacker Compo',
        categories: categories,
        users: users
      });
    });
  });
};