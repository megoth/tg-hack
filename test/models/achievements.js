
var mongoose = require("mongoose");

require("../../app/models/achievement");

var Achievement = mongoose.model("Achievement");

exports.testAchievementA = function () {
  return new Achievement({
    title: "testTitle",
      body: "testBody",
    image: "testImage",
    requirements: []
  });
};

exports.save = function (achievement, cb) {
  achievement.save(function (err, achievement) {
    Achievement.load(achievement._id, cb);
  });
};