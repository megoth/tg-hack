//noinspection JSUnresolvedFunction
/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  https = require('https'),
  markdown = require('markdown-js').markdown,
  _ = require('underscore'),
  querystring = require('querystring'),
  geekevents = require('../helpers/geekevents'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env];

/**
 * Handling login using Geekevents SSO-API
 *
 * @param req
 * @param res
 */
exports.auth = function (req, res) {
  var query = req.query,
    id = query["id"],
    timestamp = query["timestamp"],
    token = query["token"];
  if (id && timestamp && token) {
    User.findOne({ geekeventsId:id }, function (err, user) {
      if (!user) {
        return geekevents.userInfo(id, timestamp, token, {
          onSuccess:function (data) {
            // means the user isn't registered, which we now will try to do
            user = new User({
              geekeventsId:id,
              geekeventsTimestamp:timestamp,
              geekeventsToken:token
            });
            _.extend(user, data);
            res.render('geekevents/signup', {
              title:"New account",
              user:user
            });
          },
          onStatusFalse:function () {
            res.render('geekevents/login', {
              title:"Failed fetching user info"
            });
          },
          onError:function () {
            res.render('geekevents/login', {
              title:"Couldn't connect to GeekEvents..."
            });
          }
        })
      }
      return geekevents.validateUser(id, timestamp, token, {
        onSuccess: function () {
          user.geekeventsTimestamp = timestamp;
          user.geekeventsToken = token;
          user.ipAddress = req.connection.remoteAddress;
          user.userAgent = req.headers["user-agent"];
          return user.save(function (err) {
            if (err) {
              res.render('500');
            }
            res.redirect('/geekevents/?id='+id+'&timestamp='+timestamp+'&token='+token);
          });
        },
        onStatusFalse: function () {
          res.render('geekevents/login', {
            title:"Couldn't validate user"
          });
        },
        onError: function () {
          res.render('geekevents/login', {
            title:"Couldn't connect to GeekEvents..."
          });
        }
      });
    });
  } else if (Object.keys(query).length > 0) {
    res.render('geekevents/login', {
      title:'Authentication failed'
    });
  }
};

/**
 * Show login form
 */
exports.login = function (req, res) {
  res.redirect('/');
};

/**
 * User signup
 */
exports.signup = function (req, res) {
  var user = new User(req.body);
  if (!user.name || !user.username || !user.geekeventsId || !user.geekeventsTimestamp || !user.geekeventsToken) {
    return res.render('users/signup', {
      title:'One or more fields missing',
      user:user
    });
  }
  return geekevents.validateUser(user.geekeventsId, user.geekeventsTimestamp, user.geekeventsToken, {
    onSuccess:function () {
      geekevents.userHasTicket(user.geekeventsId, user.geekeventsTimestamp, user.geekeventsToken, config.geekevents.eventId, {
        onSuccess: function (numTickets) {
          user.hasTicket = numTickets !== 0;
          user.ipAddress = req.connection.remoteAddress;
          user.userAgent = req.headers["user-agent"];
          user.save(function (err) {
            if (err) {
              return res.render('500');
            }
            user.genVpn();
            return res.redirect('/geekevents/auth?id='+user.geekeventsId+'&timestamp='+user.geekeventsTimestamp+'&token='+user.geekeventsToken);
          });
        },
        onError: function () {
          res.render('users/signup', {
            title:"Couldn't connect to Geekevents (tried to check for tickets)",
            user:user
          });
        }
      });
    },
    onStatusFalse:function () {
      res.render('users/signup', {
        title:"Failed GeekEvents-validation",
        user:user
      });
    },
    onError:function () {
      res.render('users/signup', {
        title:"Couldn't connect to GeekEvents...",
        user:user
      });
    }
  });
};