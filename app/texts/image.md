# Linux image

To help you solve the VPN-assignments, you are advised to use a Linux-environment. Most of you have windows installed on your machines, and to help you set up a linux environment, we've created an image (a virtual machine) with <a href="http://www.kalilinux.net/">Kali linux</a>, that we recommend that you use.

To run the image you can use <a href="https://www.virtualbox.org/">VirtualBox</a>.

<a class="btn btn-primary" href="http://compo.tg13.gathering.org/files/KaliImg.ova">Download image</a>

Passord til nedlastingen er **hacker**