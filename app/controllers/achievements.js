/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  _ = require('underscore'),
  promise = require('node-promise'),
  markdown = require('markdown-js').markdown,
  Achievement = mongoose.model('Achievement'),
  Assignment = mongoose.model('Assignment'),
  User = mongoose.model('User');

/**
 * Find achievement by id
 */

exports.achievement = function (req, res, next, id) {
  Achievement.load(id, function (err, achievement) {
    if (err) {
      return next(err);
    }
    if (!achievement) {
      return next(new Error('Failed to load achievement ' + id));
    }
    req.achievement = achievement;
    return next();
  });
};

/**
 * Create an achievement
 */

exports.create = function (req, res) {
  var achievement = new Achievement(req.body);
  achievement.visible = !!req.body.visible;
  achievement.save(function (err) {
    if (err) {
      res.render('achievements/new', {
        title: 'New Achievement',
        achievement: achievement,
        errors: err.errors
      });
    } else {
      res.redirect('/achievements');
    }
  });
};

/**
 * Delete an achievement
 */

exports.delete = function (req, res) {
  res.render('achievements/delete', {
    title: 'Delete ' + req.achievement.title,
    achievement: req.achievement
  });
};

/**
 * Destroy an achievement
 */

exports.destroy = function (req, res) {
  var achievement = req.achievement;
  achievement.remove(function (err) {
    if (err) {
      throw new Error(err);
    }
    req.flash('notice', 'Deleted successfully');
    res.redirect('/achievements')
  });
};

/**
 * Edit an achievement
 */

exports.edit = function (req, res) {
  res.render('achievements/edit', {
    title: 'Edit ' + req.achievement.title,
    achievement: req.achievement
  });
};

/**
 * List of achievements
 */

exports.index = function (req, res) {
  Achievement.list(function (err, achievements) {
    if (err) {
      return res.render('500')
    }
    return res.render('achievements/index', {
      title: 'List of Achievements',
      achievements: achievements
    });
  });
};

/**
 * New achievement
 */

exports.new = function (req, res) {
  res.render('achievements/new', {
    title: 'New Achievement',
    achievement: new Achievement({})
  });
};

/**
 * View an achievement
 */

exports.show = function (req, res) {
  var assignmentMap = {},
    achievement = req.achievement,
    user = req.user || new User({});
  achievement.body = markdown(achievement.body);
  Assignment.list({}, function (err, assignments) {
    if (err) {
      return res.render('500');
    }
    assignments.forEach(function (assignment) {
      assignmentMap[assignment._id] = assignment;
    });
    return res.render('achievements/show', {
      title: req.achievement.title,
      achievement: achievement,
      assignmentMap: assignmentMap,
      hasAchievement: user.hasAchievement(achievement),
      user: user
    });
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.stats = function (req, res) {
  var achievement = req.achievement,
    user = req.user || new User({});
  achievement.body = markdown(achievement.body);
  User.listFromAchievement(achievement, function (err, users) {
    if (err) {
      req.flash('error', "Error while loading users");
      return res.redirect('/assignments/'+achievement._id);
    }
    return res.render('achievements/stats', {
      title: achievement.title,
      achievement: achievement,
      user: user,
      users: users,
      hasAchievement: user.hasAchievement(achievement)
    });
  });
};


/**
 * Update achievement
 */

exports.update = function (req, res) {
  var achievement = _.extend(req.achievement, req.body);
  achievement.visible = !!req.body.visible;
  achievement.save(function (err) {
    if (err) {
      res.render('achievements/edit', {
        title: 'Edit Achievement',
        achievement: achievement,
        errors: err.errors
      });
    } else {
      res.redirect('/achievements')
    }
  });
};