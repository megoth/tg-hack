
var mongoose = require("mongoose");

require("../../app/models/requirement");

var Requirement = mongoose.model('Requirement');

exports.save = function (requirement, cb) {
  requirement.save(function (err, requirement) {
    Requirement.load(requirement._id, cb);
  });
};

exports.testRequirementA = function (assignment) {
  return new Requirement({
    points: 100,
    assignments: [assignment]
  });
};