var fs = require('fs'),
  markdown = require('markdown-js').markdown,
  promise = require('node-promise'),
  renderFile = function (req, res, fileName, error) {
    fs.readFile(__dirname + "/../texts/" + fileName + ".md", function (err, content) {
      if (err) {
        req.flash("error", error);
        return res.redirect("/");
      }
      return res.render("texts/simple", {
        content:markdown("" + content)
      });
    });
  };

exports.assignments = function (req, res) {
  renderFile(req, res, "assignments", "Error loading file assignments.md");
};

exports.image = function (req, res) {
  renderFile(req, res, "image", "Error loading file image.md");
};

exports.progressGraph = function (req, res) {
  renderFile(req, res, "progressGraph", "Error loading file progressGraph.md");
};

exports.vpn = function (req, res) {
  renderFile(req, res, "vpn", "Error loading file vpn.md");
};

exports.vm = function (req, res) {
  renderFile(req, res, "vm", "Error loading file vm.md");
};