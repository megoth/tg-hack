
var mongoose = require("mongoose");

require("../../app/models/category");

var Category = mongoose.model("Category");

exports.save = function (category, cb) {
  category.save(function (err, category) {
    Category.load(category._id, cb);
  });
};

exports.testCategoryA = function () {
  return new Category({
    title: "testTitle",
    shortTitle: "test",
    abbreviation: "t",
    description: "testDescription",
    order: 0
  });
};