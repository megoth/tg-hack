module.exports = function (grunt) {
  // Project configuration
  grunt.initConfig({
    buster:{
      server:{}
    },
    compass:{
      sassDir:'sass',
      cssDir:'public/stylesheets',
      imagesDir:'public/images',
      javascriptsDir:'public/javascripts'
    },
    watch:{
      buster:{
        files:[
          "app/*/*.js",
          "test/*/*.js"
        ],
        tasks:"test"
      },
      compass:{
        files:"sass/**.scss",
        tasks:"compass"
      },
      nodemon:{
        files:"routes/*.js",
        tasks:"default"
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-plugin-buster');

  grunt.registerTask('default', 'watch');
  grunt.registerTask('test', ['buster']);
};