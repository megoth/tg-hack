/*global process, require*/

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  _ = require('underscore'),
  Schema = mongoose.Schema;

/**
 * Achievement schema
 */

var AchievementSchema = new Schema({
  title: {type: String, required: true, trim: true},
  body: {type: String, default: '', trim: true},
  image: {type: String, required: true},
  order: {type: Number, default: 0, required: true},
  visible: {type: Boolean, default: true, required: true},
  requirements: [
    {type:Schema.ObjectId, ref: 'Requirement'}
  ]
});

/**
 * Validations
 */

AchievementSchema.path('title').validate(function (title) {
  return title.length > 0
}, 'Assignment title cannot be blank');

AchievementSchema.path('body').validate(function (body) {
  return body.length > 0
}, 'Assignment body cannot be blank');

/**
 * Methods
 */

AchievementSchema.methods = {
  isVisible: function (req) {
    return this.visible ||
      req.isAuthenticated() && req.user.hasAchievement(this) ||
      req.isAdmin()
  }
};

/**
 * Pre-remove hook
 */

AchievementSchema.pre('remove', function (next) {
  this.requirements.forEach(function (requirement) {
    requirement.remove();
  });
  next();
});

/**
 * Statics
 */

AchievementSchema.statics = {

  /**
   * Find assignment by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api public
   */

  load: function (id, cb) {
    this.findOne({ _id: id })
      .populate("requirements")
      .exec(cb);
  },

  /**
   * List assignment
   *
   * @param {Function} cb
   * @api public
   */

  list: function (cb) {
    this.find()
      .populate("requirements")
      .sort({order:1})
      .exec(cb);
  }
};

mongoose.model('Achievement', AchievementSchema);
