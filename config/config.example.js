module.exports = {
  development:{
    root:require('path').normalize(__dirname + '/..'),
    app:{
      name:'TG Hacker compo site'
    },
    db:'mongodb://localhost/tg-hack-dev',
    genVpn: {
      script: 'path-to-vpn-generator-script',
      outPath: 'path-to-files'
    },
    geekevents: {
      redirect: 'your-url',
      eventId: 'event-id'
    }
  },
  test:{

  },
  production:{

  }
};
