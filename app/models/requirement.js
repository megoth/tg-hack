/*global process, require*/

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  _ = require('underscore'),
  Schema = mongoose.Schema;

var RequirementSchema = new Schema({
  points: {type: Number, required: true, default: 0},
  assignments: [
    {type: Schema.ObjectId, ref: "Assignment"}
  ]
});

/**
 * Statics
 */

RequirementSchema.statics = {

  /**
   * Find requirement by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api public
   */

  load: function (id, cb) {
    this.findOne({ _id: id })
      .populate("assignments")
      .exec(cb);
  }
};

mongoose.model('Requirement', RequirementSchema);