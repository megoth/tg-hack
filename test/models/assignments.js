
var mongoose = require("mongoose");

require("../../app/models/assignment");

var Assignment = mongoose.model('Assignment');

exports.save = function (assignment, cb) {
  assignment.save(function (err, assignment) {
    Assignment.load(assignment._id, cb);
  }.bind(this));
};

exports.testAssignmentA = function (category) {
  return new Assignment({
    title: "testTitle",
    body: "testBody",
    level: 1,
    categories: [category],
    points: 100
  });
};

exports.testAssignmentB = function (category, requirement) {
  return new Assignment({
    title: "testTitleB",
    body: "testBodyB",
    level: 1,
    categories: [category],
    requirements: [requirement],
    points: 200
  });
};