# TG Hacking Compo

The system used to serve the hacking compo at [The Gathering 2013 - Singularity](http://www.gathering.org/tg13/).

It runs on [Express](http://expressjs.com/), using [MongoDB](http://www.mongodb.org/) as storage, serving HTML and JSON that is presented with [AngularJS](http://angularjs.org/).

[Grunt](http://gruntjs.com/) and [nodemon](https://github.com/remy/nodemon) is recommended for development.

## Setup

1. Install [Node](http://nodejs.org/) and MongoDB
2. run `npm install`
3. Configure `config/config.js`
4. Run `node app`