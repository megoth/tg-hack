/*global assert, refute, require*/

var config = require('../../config/config')["test"],
  buster = require("buster"),
  sinon = require("sinon"),
  mongoose = require("mongoose"),
  promise = require("node-promise"),
  achievementSave = require('./achievements').save,
  userSave = require("./users").save,
  userSetup = require("./users").setUp,
  userTeardown = require("./users").teardown;

require("../../app/models/achievement");
require("../../app/models/assignment");
require("../../app/models/category");
require("../../app/models/requirement");
require("../../app/models/user");

buster.testCase("User model", {
  setUp: userSetup,
  tearDown: userTeardown,
  "Can be initiated": function () {
    assert.isObject(this.user);
  },
  "When checking for admin status": {
    "by default is false": function () {
      refute(this.user.isAdmin);
    },
    "but can be manipulated": function () {
      this.user.isAdmin = true;
      assert(this.user.isAdmin);
    }
  },
  "Can check for assignments": {
    "by default has none": function () {
      refute(this.user.hasAssignment(this.assignmentA));
    },
    "but changes after its added and saved": {
      setUp: function (done) {
        this.user.addAssignment(this.assignmentA, {
          onSuccess: function (user) {
            userSave(user, done(function (err, user) {
              this.user = user;
            }.bind(this)));
          }.bind(this)
        });
      },
      "assignment is now set": function () {
        assert(this.user.hasAssignment(this.assignmentA));
      }
    }
  },
  "Can validate by requirements": {
    "Assignment with no requirements are readily available": function () {
      var response = this.user.canDoAssignment(this.assignmentA);
      assert(response);
    },
    "Cannot do hard assignment at once": function () {
      var response = this.user.canDoAssignment(this.assignmentB);
      refute(response);
    },
    "After requirements are met": {
      setUp: function (done) {
        this.user.addAssignment(this.assignmentA, {
          onSuccess: function (user) {
            userSave(user, done(function (err, user) {
              this.user = user;
            }.bind(this)));
          }.bind(this)
        });
      },
      "hard one can be accessed": function () {
        var response = this.user.canDoAssignment(this.assignmentB);
        assert(response);
      }
    }
  },
  "Adding assignments": {
    setUp: function () {
      this.successSpy = sinon.spy();
      this.onAlreadySolvedSpy = sinon.spy();
      this.onRequirementsNotMetSpy = sinon.spy();
    },
    "Adding one": {
      setUp: function () {
        this.user.addAssignment(this.assignmentA, {
          onSuccess: this.successSpy,
          onAlreadySolved: this.onAlreadySolvedSpy,
          onRequirementsNotMet: this.onRequirementsNotMetSpy
        });
      },
      "is successful": function () {
        assert(this.successSpy.called);
        refute(this.onAlreadySolvedSpy.called);
        refute(this.onRequirementsNotMetSpy.called);
      },
      "adds assignment": function () {
        assert.same(this.user.assignments.length, 1);
        assert.equals(this.user.assignments[0], this.assignmentA._id);
      },
      "adds points": function () {
        assert.equals(this.user.points, 100);
      },
      "should result in category levels": function () {
        assert.equals(this.user.categories[0].level, 1);
      },
      "Adding another": {
        setUp: function (done) {
          this.successSpy = sinon.spy();
          this.onAlreadySolvedSpy = sinon.spy();
          this.onRequirementsNotMetSpy = sinon.spy();
          userSave(this.user, done(function (err, user) {
            user.addAssignment(this.assignmentB, {
              onSuccess: this.successSpy,
              onAlreadySolved: this.onAlreadySolvedSpy,
              onRequirementsNotMet: this.onRequirementsNotMetSpy
            });
            this.user = user;
          }.bind(this)));
        },
        "is successful": function () {
          assert(this.successSpy.called);
          refute(this.onAlreadySolvedSpy.called);
          refute(this.onRequirementsNotMetSpy.called);
        },
        "adds assignment": function () {
          assert.same(this.user.assignments.length, 2);
          assert.equals(this.user.assignments[1], this.assignmentB._id);
        },
        "adds points": function () {
          assert.equals(this.user.points, 300);
        },
        "should result in category levels": function () {
          assert.equals(this.user.categories[0].level, 2);
        }
      },
      "Cannot add the same": {
        setUp: function (done) {
          this.successSpy = sinon.spy();
          this.onAlreadySolvedSpy = sinon.spy();
          this.onRequirementsNotMetSpy = sinon.spy();
          userSave(this.user, done(function (err, user) {
            user.addAssignment(this.assignmentA, {
              onSuccess: this.successSpy,
              onAlreadySolved: this.onAlreadySolvedSpy,
              onRequirementsNotMet: this.onRequirementsNotMetSpy
            });
            this.user = user;
          }.bind(this)));
        },
        "fails": function () {
          refute(this.successSpy.called);
          assert(this.onAlreadySolvedSpy.called);
          refute(this.onRequirementsNotMetSpy.called);
        },
        "do not add assignment": function () {
          assert.same(this.user.assignments.length, 1);
        },
        "do not add points": function () {
          assert.equals(this.user.points, 100);
        }
      }
    },
    "Adding a too hard one": {
      setUp: function () {
        this.user.addAssignment(this.assignmentB, {
          onSuccess: this.successSpy,
          onAlreadySolved: this.onAlreadySolvedSpy,
          onRequirementsNotMet: this.onRequirementsNotMetSpy
        });
      },
      "fails": function () {
        refute(this.successSpy.called);
        refute(this.onAlreadySolvedSpy.called);
        assert(this.onRequirementsNotMetSpy.called);
      },
      "do not add assignment": function () {
        assert.same(this.user.assignments.length, 0);
      },
      "do not add points": function () {
        assert.equals(this.user.points, 0);
      }
    }
  },
  "Adding achievements": {
    "Start with no achievements": function () {
      assert.equals(this.user.achievements.length, 0);
      refute(this.user.hasAchievement(this.achievement));
    },
    "Easy achievement": {
      "is easy achievable": function () {
        assert(this.user.canGetAchievement(this.achievement));
        refute(this.user.hasAchievement(this.achievement));
      },
      "check will successful": {
        setUp: function () {
          this.onSuccessSpy = sinon.spy();
          this.onAlreadyAchievedSpy = sinon.spy();
          this.onRequirementFailed = sinon.spy();
          this.user.addAchievement(this.achievement, {
            onSuccess: this.onSuccessSpy,
            onAlreadyAchieved: this.onAlreadyAchievedSpy,
            onRequirementFailed: this.onRequirementFailed
          });
        },
        "easily": function () {
          assert(this.onSuccessSpy.called);
          refute(this.onAlreadyAchievedSpy.called);
          refute(this.onRequirementFailed.called);
        }
      },
      "can add at once": {
        setUp: function (done) {
          this.onSuccessSpy = sinon.spy();
          this.onAlreadyAchievedSpy = sinon.spy();
          this.onRequirementFailed = sinon.spy();
          this.user.addAchievement(this.achievement, {
            onSuccess: function (user) {
              userSave(user, done(function (err, user) {
                this.user = user;
              }.bind(this)));
            }.bind(this)
          });
        },
        "easily": function () {
          assert.equals(this.user.achievements.length, 1);
          assert(this.user.hasAchievement(this.achievement));
        },
        "should result in a level": function () {
          assert.equals(this.user.level, 1);
        },
        "cannot be added twice": function () {
          this.onSuccessSpy = sinon.spy();
          this.onAlreadyAchievedSpy = sinon.spy();
          this.onRequirementFailed = sinon.spy();
          this.user.addAchievement(this.achievement, {
            onSuccess: this.onSuccessSpy,
            onAlreadyAchieved: this.onAlreadyAchievedSpy,
            onRequirementFailed: this.onRequirementFailed
          });
          refute(this.onSuccessSpy.called);
          assert(this.onAlreadyAchievedSpy.called);
          refute(this.onRequirementFailed.called);
        }
      }
    },
    "Hard achievement": {
      setUp: function (done) {
        this.achievement.requirements.push(this.requirement);
        achievementSave(this.achievement, done(function (err, achievement) {
            this.achievement = achievement;
        }.bind(this)));
      },
      "user does not meet requirements": function () {
        refute(this.user.canGetAchievement(this.achievement));
      },
      "not enough with just points": function (done) {
        this.user.points = 100;
        userSave(this.user, done(function (err, user) {
          refute(user.canGetAchievement(this.achievement));
        }.bind(this)));
      },
      "not enough with just assignment": function (done) {
        this.user.assignments.push(this.assignmentA);
        userSave(this.user, done(function (err, user) {
          refute(user.canGetAchievement(this.achievement));
        }.bind(this)));
      },
      "but enough with both": {
        setUp: function (done) {
          this.user.points = 100;
          this.user.assignments.push(this.assignmentA);
          userSave(this.user, done(function (err, user) {
            this.user = user;
          }.bind(this)));
        },
        "achievement is available": function () {
          assert(this.user.canGetAchievement(this.achievement));
        }
      }
    }
  }
});