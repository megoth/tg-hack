(function ($, d3) {
  $.fn.spider = function (data) {
    var width = 200,
      height = 200,
      outerRadius = height / 2 - 10,
      innerRadius = 20;

    var angle = d3.time.scale()
      .range([0, 2 * Math.PI]);

    var radius = d3.scale.linear()
      .range([innerRadius, outerRadius]);

    var z = d3.scale.category20b();

    var stack = d3.layout.stack()
      .offset("zero")
      .values(function (d) {
        return d.values;
      })
      .x(function (d) {
        return d.time;
      })
      .y(function (d) {
        return d.value;
      });

    var nest = d3.nest()
      .key(function (d) {
        return d.key;
      });

    var line = d3.svg.line.radial()
      .interpolate("linear-closed") // evt cardinal-closed
      .angle(function (d) {
        return angle(d.time);
      })
      .radius(function (d) {
        return radius(d.y0 + d.y);
      });

    var area = d3.svg.area.radial()
      .interpolate("linear-closed") // evt cardinal-closed
      .angle(function (d) {
        return angle(d.time);
      })
      .innerRadius(function (d) {
        return radius(d.y0);
      })
      .outerRadius(function (d) {
        return radius(d.y0 + d.y);
      });

    var svg = d3.select("#UserSpider").append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var graph = function (categories) {
      var layers = stack(nest.entries(categories));

      // Extend the domain slightly to match the range of [0, 2π].
      angle.domain([0, d3.max(categories, function (d) {
        return d.time + 1;
      })]);
      radius.domain([0, d3.max(categories, function (d) {
        return d.y0 + d.y;
      })]);

      svg.selectAll(".layer")
        .data(layers)
        .enter().append("path")
        .attr("class", "layer")
        .attr("d", function (d) {
          return area(d.values);
        })
        .style("fill", function (d, i) {
          return z(i);
        });

      svg.selectAll(".axis")
        .data(d3.range(angle.domain()[1]))
        .enter().append("g")
        .attr("class", "axis")
        .attr("transform", function (d) {
          return "rotate(" + angle(d) * 180 / Math.PI + ")";
        })
        .call(d3.svg.axis()
        .scale(radius.copy().range([-innerRadius, -outerRadius]))
        .orient("right")
        .tickValues([])
        .tickSize(3, 1))
        .append("text")
        .attr("y", outerRadius * -1)
        .attr("text-anchor", "middle")
        .attr("title", function (d, i) {
          return categories[i * 2].title;
        })
        .style("font-size", 10)
        .text(function (d, i) {
          return categories[i * 2].abbr;
        });
    };


    var level = function (level) {
      svg.append("text")
        .attr("text-anchor", "middle")
        .attr("y", 5)
        .attr("title", "Hacker level")
        .text(level);
    };
    return this.each(function () {
      console.log(this, data);
      graph(data.categories);
      level(data.level);
    });
  };
}(jQuery, d3));