/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  User = mongoose.model('User');

/**
 *
 * @param req
 * @param res
 */
exports.delete = function (req, res) {
  res.render('profiles/delete', {
    title: 'Delete profile',
    user: req.user
  });
};

/**
 * Destroy a user
 */

exports.destroy = function (req, res) {
  req.user.remove(function (err) {
    if (err) {
      req.flash("error", "Couldn't delete profile");
      return req.redirect('/profile/delete')
    }
    req.flash('success', 'Profile deleted successfully');
    return res.redirect('/');
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.edit = function (req, res) {
  res.render('profiles/edit', {
    user: req.user
  })
};

exports.update = function (req, res) {
  // in case someone tries to be a smartass, does a whitelist processing of req.body
  var user = req.user;
  user.private = !!req.body.private;
  user.save(function (err) {
    if (err) {
      req.flash("error", "Something went wrong when trying to save profile");
      return res.redirect("/profile/edit");
    }
    req.flash("success", "Profile updated");
    return res.redirect('/profile');
  });
};

exports.view = function (req, res) {
  res.render('profiles/show', {
    user: req.user
  });
};