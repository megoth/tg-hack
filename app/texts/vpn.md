# VPN client

Some assignments require you to access our system with OpenVpn.


All you need to do is download the configuration files, extract, and run:

    openvpn hacker.conf 

<a class="btn btn-primary" href="/vpn/download">Download configuration</a>

Pro tip: You need a Linux distro, we recommend [Kali Linux](http://www.kali.org/)