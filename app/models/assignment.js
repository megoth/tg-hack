/*global process, require*/

/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  _ = require('underscore'),
  Schema = mongoose.Schema;

/**
 * Assignment schema
 */

var AssignmentSchema = new Schema({
  title: {type: String, required: true},
  level: {type: Number, default: 0, required: true},
  body: {type: String, default: '', trim: true, required: true},
  notes: {type: String, default: '', trim: true},
  categories: [
    {type: Schema.ObjectId, ref: 'Category'}
  ],
  requirements: [
    {type: Schema.ObjectId, ref: 'Assignment'}
  ],
  token: {type: String, default: '', trim: true},
  order: {type: Number, default: 0, required: true},
  points: {type: Number, default: 0}
});

/**
 * Validations
 */

AssignmentSchema.path('title').validate(function (title) {
  return title.length > 0
}, 'Assignment title cannot be blank');

AssignmentSchema.path('body').validate(function (body) {
  return body.length > 0
}, 'Assignment body cannot be blank');

AssignmentSchema.path('categories').validate(function (categories) {
  return categories.length > 0
}, 'Assignment categories must contain at least one category');

AssignmentSchema.path('points').validate(function (points) {
  return points > 0
}, 'Assignment categories must contain at least one category');

/**
 * Methods
 */
AssignmentSchema.methods = {
  getCategoryClass: function () {
    if (this.level === 2) {
      return "mixed";
    }
    if (this.level === 3) {
      return "final";
    }
    return "default";
  }
};

/**
 * Statics
 */

AssignmentSchema.statics = {

  /**
   * Find assignment by id
   *
   * @param {ObjectId} id
   * @param {Function} cb
   * @api public
   */

  load: function (id, cb) {
    this.findOne({ _id: id })
      .populate('categories')
      .populate('requirements')
      .exec(cb);
  },

  /**
   * List assignment
   *
   * @param criteria
   * @param {Function} cb
   * @api public
   */

  list: function (criteria, cb) {
    this.find(criteria)
      .populate('categories', 'title, abbreviation')
      .populate('requirements', 'title')
      .sort({'title': 1})
      .exec(cb);
  },

  listFromCategory: function (categoryId, cb) {
    this
      .find({level:1})
      .where('level').equals(1)
      .where('categories').in([categoryId])
      .populate('requirements', 'title')
      .sort({'order': 1})
      .exec(cb);
  },

  /**
   *
   * @param assignment
   * @param cb
   */
  listRequirement: function (assignment, cb) {
    this.find()
      .exec(function (err, requirements) {
        requirements.forEach(function (requirement) {
          requirement.checked = _.where(assignment.requirements, { id: requirement.id }).length > 0;
        });
        cb(requirements);
      });
  }
};

mongoose.model('Assignment', AssignmentSchema);
