/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  _ = require('underscore'),
  promise = require('node-promise'),
  Achievement = mongoose.model('Achievement'),
  Assignment = mongoose.model('Assignment'),
  Requirement = mongoose.model('Requirement');

/**
 * Find requirement by id
 */

exports.requirement = function (req, res, next, id) {
  Requirement.load(id, function (err, requirement) {
    if (err) {
      return next(err);
    }
    if (!requirement) {
      return next(new Error('Failed to load requirement ' + id));
    }
    req.requirement = requirement;
    next();
  });
};

/**
 * Add a category to the requirements
 * @param req
 * @param res
 */
exports.addAssignment = function (req, res) {
  Assignment.list({}, function (err,  assignments) {
    if (err) {
      return res.render('500');
    }
    return res.render('requirements/addAssignment', {
      achievement: req.achievement,
      requirement: req.requirement,
      assignments: assignments
    });
  });
};

/**
 * Create an requirement
 */

exports.create = function (req, res) {
  var achievement = req.achievement,
    requirement = new Requirement(req.body),
    achievementPromise = new promise.Promise(),
    requirementPromise = new promise.Promise();

  achievement.requirements.push(requirement);

  achievement.save(achievementPromise.resolve);
  requirement.save(requirementPromise.resolve);

  promise.all(achievementPromise, requirementPromise)
    .then(function () {
      res.redirect('/achievements/' + achievement._id);
    });
};

/**
 * Delete an requirement
 */

exports.delete = function (req, res) {
  res.render('requirements/delete', {
    title:'Delete requirement',
    achievement:req.achievement,
    requirement:req.requirement
  });
};

/**
 * Destroy an requirement
 */

exports.destroy = function (req, res) {
  var achievement = req.achievement,
    requirement = req.requirement,
    achievementPromise = new promise.Promise(),
    requirementPromise = new promise.Promise(),
    requirementIndex = _.indexOf(achievement.requirements, requirement);
  achievement.requirements.splice(requirementIndex, 1);

  achievement.save(achievementPromise.resolve);
  requirement.remove(requirementPromise.resolve);

  promise.all(achievementPromise, requirementPromise)
    .then(function () {
      req.flash('notice', 'Deleted successfully');
      res.redirect('/achievements/' + achievement._id);
    });
};

/**
 * Edit an achievement
 */

exports.edit = function (req, res) {
  res.render('requirements/edit', {
    title:'Edit requirement',
    achievement:req.achievement,
    requirement:req.requirement
  });
};

/**
 * New achievement
 */

exports.new = function (req, res) {
  res.render('requirements/new', {
    title:'New Achievement',
    achievement:req.achievement,
    requirement:new Requirement({}),
    requirementIndex:null
  });
};

/**
 *
 * @param req
 * @param res
 */
exports.pushAssignment = function (req, res) {
  var assignment = new Assignment(req.body),
    requirement = req.requirement;
  requirement.assignments.push(assignment);
  requirement.save(function (err) {
    if (err) {
      res.render('500');
    }
    res.redirect('/achievements/' + req.achievement._id);
  });
};

/**
 * Update requirement
 */

exports.update = function (req, res) {
  var achievement = req.achievement,
    requirement = _.extend(req.requirement, req.body);
  requirement.save(function (err) {
    if (err) {
      res.render('requirements/edit', {
        title:'Edit Requirement',
        achievement:achievement,
        requirement:requirement,
        errors:err.errors
      });
    } else {
      res.redirect('/achievements/' + achievement._id)
    }
  });
};
