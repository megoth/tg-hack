var facebookInit = function (appId, appName, channelUrl) {
  window.fbAsyncInit = function () {
    FB.init({
      appId:appId,
      channelUrl:channelUrl,
      status:true,
      cookie:true,
      xfbml:true
    });
  };
  (function (d, debug) {
    var js, id = appName, ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
    ref.parentNode.insertBefore(js, ref);
  }(document, false));
};