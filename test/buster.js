var exports = module.exports,
  config = require('../config/config')["test"],
  mongoose = require("mongoose");

// setup db
mongoose.connect(config.db);

exports["server"] = {
    rootPath: "../",
    environment: "node",
    sources: [
        "app/models/*.js"
    ],
    tests: [
        "test/*/*-test.js"
    ]
};