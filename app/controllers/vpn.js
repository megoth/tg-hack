
var env = process.env.NODE_ENV || 'development',
  config = require('../../config/config')[env],
  fs = require('fs');

exports.download = function (req, res) {
  var filePath = config.genVpn.outPath+'/'+req.user.geekeventsId+'.tar.gz';
  fs.readFile(filePath, function (err, data) {
    if (err) {
      return res.render('500');
    }
    res.writeHead(200, {
      'Content-type': 'application/octet-stream',
      'Content-disposition': 'attachment; filename=hackportal.tar.gz'
    });
    return res.end(data);
  });
};