var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  GeekeventsStrategy = require('./middlewares/geekevents/strategy');

module.exports = function (passport, config) {
  // require('./initializer')

  // serialize sessions
  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.load(id, done)
  });

  // use geekevents strategy
  passport.use(new GeekeventsStrategy(function (userCred, done) {
    User.findOne(userCred, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, { message:'Unknown user' });
      }
      return done(null, user)
    })
  }));
};
