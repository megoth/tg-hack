
var mongoose = require("mongoose"),
  promise = require("node-promise"),
  testAchievementA = require("./achievements").testAchievementA,
  achievementSave = require("./achievements").save,
  testAssignmentA = require("./assignments").testAssignmentA,
  testAssignmentB = require("./assignments").testAssignmentB,
  assignmentSave = require("./assignments").save,
  testCategoryA = require("./categories").testCategoryA,
  categorySave = require("./categories").save,
  testRequirementA = require("./requirements").testRequirementA,
  requirementSave = require("./requirements").save;

require("../../app/models/user");

var User = mongoose.model('User');

exports.testUserA = function () {
  return new User({
    name: "testName",
    username: "testUsername",
    geekeventsId: 1337,
    geekeventsTimestamp: 666,
    geekeventsToken: "testToken",
    ipAddress: "testIpAddress",
    userAgent: "testUserAgent",
    hasTicket: true
  });
};

exports.save = function (user, cb) {
  user.save(function (err, user) {
    User.load(user._id, cb);
  });
};

exports.setUp = function (cb) {
  // setup models
  var Achievement = mongoose.model('Achievement'),
    Assignment = mongoose.model('Assignment'),
    Category = mongoose.model('Category'),
    Requirement = mongoose.model('Requirement'),
    User = mongoose.model('User');
  // do all the other stuff
  var achievements = new promise.Promise(),
    assignments = new promise.Promise(),
    categories = new promise.Promise(),
    requirements = new promise.Promise(),
    users = new promise.Promise();
  achievementSave(testAchievementA(), function (err, achievement) {
    this.achievement = achievement;
    achievements.resolve(achievement);
  }.bind(this));
  categorySave(testCategoryA(), function (err, category) {
    this.category = category;
    categories.resolve(category);
    assignmentSave(testAssignmentA(category), function (err, assignmentA) {
      this.assignmentA = assignmentA;
      assignmentSave(testAssignmentB(category, assignmentA), function (err, assignmentB){
        this.assignmentB = assignmentB;
        assignments.resolve();
      }.bind(this));
      requirementSave(testRequirementA(assignmentA), function (err, requirement) {
        this.requirement = requirement;
        requirements.resolve(requirement);
      }.bind(this));
    }.bind(this));
  }.bind(this));
  exports.save(exports.testUserA(), function (err, user) {
    this.user = user;
    users.resolve(user);
  }.bind(this));
  promise.all(users, achievements, assignments, categories, requirements).then(cb);
};

exports.teardown = function (cb) {
  // setup models
  var achievements = new promise.Promise(),
    assignments = new promise.Promise(),
    categories = new promise.Promise(),
    requirements = new promise.Promise(),
    users = new promise.Promise();
  mongoose.model('Achievement').remove(function () {
    achievements.resolve();
  });
  mongoose.model('Assignment').remove(function () {
    assignments.resolve();
  });
  mongoose.model('Category').remove(function () {
    categories.resolve();
  });
  mongoose.model('Requirement').remove(function () {
    requirements.resolve();
  });
  mongoose.model('User').remove(function () {
    users.resolve();
  });
  promise.all(achievements, assignments, categories, requirements, users).then(cb);
};