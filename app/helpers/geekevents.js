var https = require('https'),
  _ = require('underscore'),
  querystring = require('querystring');

exports.userHasTicket = function (user_id, timestamp, token, event_id, fns) {
  fns = _.extend({
    onSuccess:function () {
    },
    onError:function () {
    }
  }, fns);

  // prepare data
  var numTickets = 0,
    post_data = querystring.stringify({
    "user_id":user_id,
    "timestamp":timestamp,
    "token":token,
    "event_id":event_id
  }),
    req = https.request({
      host:'www.geekevents.org',
      path:'/sso/user-has-ticket/',
      method:'POST',
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Content-Length':post_data.length
      }
    }, function (res) {
      res.on('data', function (data) {
        console.log("DATA RETRIEVED FROM GE", "" + data);
        numTickets = parseInt("" + data);
        return fns.onSuccess(numTickets);
      });
    });
  req.on("error", fns.onError);

  console.log(post_data);

  // post data
  req.write(post_data);
  req.end();
};

exports.userInfo = function (user_id, timestamp, token, fns) {
  // some fallback
  fns = _.extend({
    onSuccess:function () {
    },
    onStatusFalse:function () {
    },
    onError:function () {
    }
  }, fns);

  // prepare data
  var post_data = querystring.stringify({
      "user_id":user_id,
      "timestamp":timestamp,
      "token":token
    }),
    req = https.request({
      host:'www.geekevents.org',
      path:'/sso/userinfo/',
      method:'POST',
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Content-Length':post_data.length
      }
    }, function (res) {
      res.on('data', function (data) {
        data = JSON.parse(data);
        if (!data.status) {
          return fns.onStatusFalse(data);
        }
        return fns.onSuccess(data);
      });
    });
  req.on("error", fns.onError);

  // post data
  req.write(post_data);
  req.end();
};

exports.validateUser = function (user_id, timestamp, token, fns) {
  // some fallback
  fns = _.extend({
    onSuccess:function () {
    },
    onStatusFalse:function () {
    },
    onError:function () {
    }
  }, fns);

  // prepare data
  var post_data = querystring.stringify({
      user_id:user_id,
      timestamp:timestamp,
      token:token
    }),
    req = https.request({
      host:'www.geekevents.org',
      path:'/sso/validate/',
      method:'POST',
      headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Content-Length':post_data.length
      }
    }, function (res) {
      res.on('data', function (data) {
        data = JSON.parse(data);
        if (!data.status) {
          return fns.onStatusFalse(data);
        }
        return fns.onSuccess(data);
      });
    });
  req.on("error", fns.onError);

  // post data
  req.write(post_data);
  req.end();
};