/**
 * Module dependencies.
 */
var passport = require('passport'),
  util = require('util'),
  BadRequestError = require('./errors/badrequesterror'),
  _ = require('underscore');


/**
 * `Strategy` constructor.
 *
 * The geekevents authentication strategy authenticates requests based on the
 * credentials submitted through an HTML-based login form.
 *
 * Applications must supply a `verify` callback which accepts `id`, `timestamp` and
 * `token` credentials, and then calls the `done` callback supplying a
 * `user`, which should be set to `false` if the credentials are not valid.
 * If an exception occured, `err` should be set.
 *
 * Optionally, `options` can be used to change the fields in which the
 * credentials are found.
 *
 * Options:
 *   - `geekeventsIdField`  field name where the id is found, defaults to _id_
 *   - `geekeventsTimestampField`  field name where the timestamp is found, defaults to _timestamp_
 *   - `geekeventsTokenField`  field name where token is found, default to _token_
 *   - `passReqToCallback`  when `true`, `req` is the first argument to the verify callback (default: `false`)
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options, verify) {
  if (typeof options == 'function') {
    verify = options;
    options = {};
  }
  if (!verify) {
    throw new Error('Geekevents authentication strategy requires a verify function');
  }

  this._geekeventsIdField = options.geekeventsIdField || 'id';
  this._geekeventsTimestampField = options.geekeventsTimestampField || 'timestamp';
  this._geekeventsTokenField = options.geekeventsTokenField || 'token';

  passport.Strategy.call(this);
  this.name = 'geekevents';
  this._verify = verify;
  this._passReqToCallback = options.passReqToCallback;
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a form submission.
 *
 * @param {Object} req
 * @api protected
 * @param options
 */
Strategy.prototype.authenticate = function(req, options) {
  // private variables
  options = _.extend({
    badRequestMessage: 'Missing credentials'
  }, options);
  var id = lookup(req.body, this._geekeventsIdField) || lookup(req.query, this._geekeventsIdField),
    timestamp = lookup(req.body, this._geekeventsTimestampField) || lookup(req.query, this._geekeventsTimestampField),
    token = lookup(req.body, this._geekeventsTokenField) || lookup(req.query, this._geekeventsTokenField),
    userCred = {
      geekeventsId: id,
      geekeventsTimestamp: timestamp,
      geekeventsToken: token,
      ipAddress: req.connection.remoteAddress,
      userAgent: req.headers["user-agent"]
    };

  if (!id || !timestamp || !token) {
    return this.fail(new BadRequestError(options.badRequestMessage));
  }

  // private functions
  function verified(err, user, info) {
    if (err) {
      return self.error(err);
    }
    if (!user) {
      return self.fail(info);
    }
    return self.success(user, info);
  }

  function lookup(obj, field) {
    if (!obj) { return null; }
    var chain = field.split(']').join('').split('[');
    for (var i = 0, len = chain.length; i < len; i++) {
      var prop = obj[chain[i]];
      if (typeof(prop) === 'undefined') { return null; }
      if (typeof(prop) !== 'object') { return prop; }
      obj = prop;
    }
    return null;
  }

  var self = this;

  if (self._passReqToCallback) {
    this._verify(req, userCred, verified);
  } else {
    this._verify(userCred, verified);
  }
};


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;